  var express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'), //mongo connection
      bodyParser = require('body-parser'), //parses information from POST
      methodOverride = require('method-override'); //used to manipulate POST
  jwt = require('jsonwebtoken'),
      mongoose = require('mongoose'), //mongo connection
      config = require('config'),
      functionCheck = require('../routes/functionCheck'),
      logger = require('../config/logger');


  router.use(bodyParser.urlencoded({
      extended: true
  }))
  router.use(methodOverride(function(req, res) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method
          delete req.body._method
          return method
      }
  }))


  var currentdate = Date.Now;
  definitions : 
	  PatientStudy :
		  type : Object
		  properties :
			  patientname :
				  type : String
			  address : 
				  type : String
			  city :
				  type : Boolean
		      pincode :
				  type : String
			  zipstudyfilepath :
				  type : Date
			  age : 
			  	  type : String
			  agetype : 
				  type : Date
			  gender :
			  	  type : Boolean
			  dateofbirth :
			      type : String
			  mobilenumber :
				  type : String
			  emailaddress : 
				  type : String
			  historypath :
				  type : String
			  studyreportfilepath :
				  type : String
			  history : 
				  type : String
			  priliminaryreport :
				  type : String
			  centerid : 
				  type : String
			  operatorid : 
				  type : String
			  physicianname :
				  type : String
			  physicianhospitalname :
				  type : String
			  studydate : 
				  type : String
			  studyorgan :
				  type : String				  
			  studystatus :
				  type : String
			  referringphysicianname :
				  type : String
			  referringphysicianhospitalname :
				  type : String
			  comments :
				  type : String
			  description :
				  type : String
			  studyfilename :
				  type : String
		      studyfilestatus :
		    	  type : String
			  studyfileuploadedtime :
				  type : String
				  
  //GET all blobs
  //functionCheck.checkPermission,
				  //TODO add condition that should able to see studies only to operator and admin
  router.get('/searchPatientStudies', functionCheck.checkToken, function(req, res, next) {
      //retrieve all blobs from Monogo
      console.log('Inside searchPatientStudies');

      mongoose.model('PatientStudy').find({
          isdelete: false
      }).
      sort({
          studyfileuploadedtime: 'desc'
      }).
      populate('studyreportid').exec(function(err, patientstudies) {

          if (err) {
              logger.error('Inside PatientStudy ::getPatientStudy::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: err
                  }

              });
          } else {
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              logger.info('Inside PatientStudy ::getPatientStudy::success');
              logger.info("getting PatientStudy as ::" + patientstudies);
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              res.json({
                  header: {
                      statuscode: 200,
                      statusmessage: "success"
                  },
                  data: {
                      patientstudies: patientstudies
                  }
              });
          }
      })

  });
  router.get('/searchPatientStudiesByUserWise/:createdby', functionCheck.checkToken, function(req, res, next) {
      //retrieve all blobs from Monogo
      console.log('Inside searchPatientStudiesByUserWise');
      
     // var createdbyUser = req.params.createdby;
      
     // console.log('getting created by as :' +createdbyUser);

      mongoose.model('PatientStudy').find({
          isdelete: false,
          createdby : req.params.createdby
      }).
      sort({
          studyfileuploadedtime: 'desc'
      }).
      populate('studyreportid').exec(function(err, patientstudies) {

          if (err) {
              logger.error('Inside PatientStudy ::getPatientStudy::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: err
                  }

              });
          } else {
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              logger.info('Inside PatientStudy ::getPatientStudy::success');
              logger.info("getting PatientStudy as ::" + patientstudies);
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              res.json({
                  header: {
                      statuscode: 200,
                      statusmessage: "success"
                  },
                  data: {
                      patientstudies: patientstudies
                  }
              });
          }
      })

  });
  router.post('/searchStudiesImages', functionCheck.checkToken, functionCheck.checkPermission, function(req, res, next) {
      console.log('Inside PatientStudy:::searchStudiesImages');
      //retrieve all blobs from Monogo
      mongoose.model('StudyImageFile').findOne({
          studyid: req.body.studyid
      }, function(err, studyimages) {
          console.log('inside get studyimages details');
          if (err) {
              logger.error('Inside StudyImageFile ::getstudyimages::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: err
                  }

              });
          } else {
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              logger.info('Inside StudyImageFile ::getStudyImageFile::success');
              logger.info("getting StudyImageFile as ::" + studyimages);
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              res.json({
                  header: {
                      statuscode: 200,
                      statusmessage: "success"
                  },
                  data: {
                      studyimages: studyimages
                  }
              });
          }
      });
  });
  //POST a new blob
  router.post('/addPatientStudyImageInfo', functionCheck.checkToken, functionCheck.checkPermission, function(req, res) {
      logger.info('Inside Patient :: addPatientStudyImageInfo');
      // Get values from POST request. These can be done through forms or REST calls. These rely on the "name" attributes for forms

      console.log('getting age as ::' + req.body.age + ':::getting age typeas ::' + req.body.agetype);
      var createdby1 = req.body.createdby;

      var fullname = req.body.fullname;
      var address = req.body.address;
      var city = req.body.city;
      var pincode = req.body.pincode;
      var age = req.body.age;
      var agetype = req.body.agetype;
      var gender = req.body.gender;
      var template = req.body.template;
      var dateofbirth = req.body.dateofbirth;
      var mobilenumber = req.body.mobilenumber;
      var emailaddress = req.body.emailaddress;
      var history = req.body.history;
      var priliminaryreport = req.body.priliminaryreport;
      var comments = req.body.comments;
      var studydate = currentdate;
      var studyorgan = req.body.studyorgan;
      var studystatus = req.body.studystatus;
      var referringphysicianname = req.body.referringphysicianname;
      var referringphysicianhospitalname = req.body.referringphysicianhospitalname;
      var comments = req.body.comments;
      var description = req.body.description;
      var createdby = createdby1;
      var createdon = currentdate;
      //TODO add later by taking user id from login user
      // console.log('created by as ::' + req.body.createdby);


      //call the create function for our database
      mongoose.model('PatientStudy').create({
          patientname: fullname,
          address: address,
          city: city,
          pincode: pincode,
          age: age,
          agetype: agetype,
          gender: gender,
          dateofbirth: dateofbirth,
          emailaddress: emailaddress,
          mobilenumber: mobilenumber,
          history: history,

          priliminaryreport: priliminaryreport,
          studydate: studydate,
          studyorgan: studyorgan,
          studystatus: studystatus,
          referringphysicianname: referringphysicianname,
          referringphysicianhospitalname: referringphysicianhospitalname,
          comments: comments,
          description: description,
          createdby: createdby1,
          createdon: createdon


      }, function(err, patientstudy) {
          if (err) {
              logger.error('inside patientstudy::createpatientstudy::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: "There was a problem adding the information to the database." + err
                  }
              });
          } else {
              logger.info('Inside patientstudy::addpatientstudy::Success' + patientstudy);
              //          res.json({
              // header :{ statuscode : 200 , statusmessage : "success"},
              // data : {patient : patient}    
              //add study information ferom here
              console.log('successfully created patientstudy as ::' + patientstudy);

              logger.info('Inside PAtientStudy::addPatientStudy::Success' + patientstudy);
              res.json({
                  header: {
                      statuscode: 200,
                      statusmessage: "success"
                  },
                  data: {
                      patientstudy: patientstudy
                  }
              });
          }
      })
  });
  //TODO remove patient info if there is problem in storing study information
  // route middleware to validate :id
  router.param('userid', function(req, res, next, userid) {
      console.log('validating ' + userid + ' exists');
      //find the ID in the Database
      mongoose.model('User').find({
          userid: userid
      }, function(err, user) {
          if (err) {
              console.log(userid + ' was not found');
              res.status(404)
              var err = new Error('Not Found');
              err.status = 404;
              res.format({
                  html: function() {
                      next(err);
                  },
                  json: function() {
                      res.json({
                          message: err.status + ' ' + err
                      });
                  }
              });
          } else {

              // object of the user
              req.userid = userid;
              // go to the next thing
              next();
              console.log('user find one successfully::::::::::::::' + user);
          }

      });

  });


  router.get('/:userid/new', function(req, res) {
      console.log('user id in new' + req.params.userid);
      res.render('patients/new', {
          title: 'Add New patient',
          "userid": req.params.userid
      });
  });

  router.param('id', function(req, res, next, id) {
      //console.log('validating ' + id + ' exists');
      //find the ID in the Database
      mongoose.model('Patient').findById(id, function(err, patient) {
          //if it isn't found, we are going to repond with 404
          if (err) {
              logger.error('Inside Patient :: ValidatePatientID ::Error::Not Found' + id);
              res.json({
                  header: {
                      statuscode: 404,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: "Patient Not Found"
                  }
              });

              //if it is found we continue on
          } else {
              //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
              //console.log(blob);
              // once validation is done save the new item in the req
              req.id = id
              logger.info('inside Patient:::ValidatePatientID:::Success'); // go to the next thing
              next();
          }
      });
  });

  router.route('/:id')
      .get(function(req, res) {
          mongoose.model('Patient').findById(req.id, function(err, patient) {
              if (err) {
                  logger.error('Inside Patient::ValidatePatientID ::Error' + err);
                  res.json({
                      header: {
                          statuscode: 400,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: "Error in validate Center ID"
                      }
                  });
              } else {
                  logger.info('Inside Patient::ValidatePatientID::Success');
                  /* var centeradd = center.dob.toISOString();
                   centerdob = centerdob.substring(0, centerdob.indexOf('T'))*/
                  res.json({
                      header: {
                          statuscode: 200,
                          statusmessage: "success"
                      },
                      data: {
                          patient: patient
                      }
                  });
              }
          });
      });

  router.route('/:id/edit')
      //GET the individual center by Mongo ID
      .get(function(req, res) {
          //search for the center within Mongo
          mongoose.model('Patient').findById(req.id, function(err, patient) {
              if (err) {
                  logger.error('Inside Patient::updatePatientInfo::Error in getting patientid' + err);
                  res.json({
                      header: {
                          statuscode: 404,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: "Patient doese not exist "
                      }
                  });
              } else {
                  //Return the center
                  logger.info('Inside Patient::UpdatePatientInfo::Success::get patient');
                  console.log('Inside Patient::UpdatePatientInfo::Success::get patient' + patient);

                  res.json({
                      header: {
                          statuscode: 200,
                          statusmessage: "success"
                      },
                      data: {
                          patient: patient
                      }
                  });
              }
          });
      })
      //PUT to update a center by ID
      .put(function(req, res) {
          // Get our REST or form values. These rely on the "name" attributes
          logger.info('inside Patient ::updatePatientInfo::assigning values');
          var fullname = req.body.fullname;
          var address = req.body.address;
          var city = req.body.city;
          var pincode = req.body.pincode;
          var age = req.body.age;
          var dateofbirth = req.body.dateofbirth;
          var mobilenumber = req.body.mobilenumber;
          var emailaddress = req.body.emailaddress;
          var history = req.body.history;
          var priliminaryreport = req.body.priliminaryreport;
          var comments = req.body.comments;

          //find the document by ID
          mongoose.model('Patient').findById(req.id, function(err, patient) {
              //update it
              patient.update({
                  fullname: fullname,
                  address: address,
                  city: city,
                  pincode: pincode,
                  age: age,
                  dateofbirth: dateofbirth,
                  emailaddress: emailaddress,
                  mobilenumber: mobilenumber,
                  history: history,
                  priliminaryreport: priliminaryreport,
                  comments: comments

              }, function(err, patientID) {
                  if (err) {
                      logger.error('Inside Center::updateCenterInfo::Error' + err);

                      res.json({
                          header: {
                              statuscode: 404,
                              statusmessage: "failure"
                          },
                          data: {
                              errormessage: "There was a problem updating the information to the database:" + err
                          }
                      });

                  } else {
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      logger.info('Inside Patient ::UpdatePatientInfo::Success');
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      res.json({
                          header: {
                              statuscode: 200,
                              statusmessage: "success"
                          },
                          data: {
                              patient: patient
                          }
                      });
                  }
              })
          });
      });
  //DELETE a center by ID
  router.delete('/deletePatient', function(req, res) {
      //find center by ID
      logger.info('Inside Patient::deletePatient');
      mongoose.model('Patient').findOneAndRemove({
          _id: req.body._id
      }, function(err, patient) {
          if (err) {
              logger.error('Inside Patient ::deletePatient::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: err
                  }

              });
          } else {
              mongoose.model('Patient').find({}, function(err, patients) {
                  console.log('inside get patient details');
                  if (err) {
                      logger.error('Inside Patient ::getPAtient::Error' + err);
                      res.json({
                          header: {
                              statuscode: 400,
                              statusmessage: "failure"
                          },
                          data: {
                              errormessage: err
                          }

                      });
                  } else {
                      //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                      logger.info('Inside PAtient ::getPatient::success');
                      logger.info("getting patients as ::" + patients);
                      //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                      res.json({
                          header: {
                              statuscode: 200,
                              statusmessage: "success"
                          },
                          data: {
                              patients: patients
                          }
                      });
                  }
              });

          }

      });
  });

  var editStudyForKeyword = function(studyid, keywordid, userid) {
      logger.info('inside Patient ::updatePatientInfo::assigning values');
      var keywordid = keywordid;
      var updatedby = userid;
      var updateddate = Date.now();

      mongoose.model('PatientStudy').findById(studyid, function(err, patientstudy) {
          //update it
          patientstudy.update({
              keywordid: keywordid,
              updatedby: updatedby,
              updateddate: updateddate
          }, function(err, patientstudy) {
              if (err) {
                  console.log('Inside Update PatientStudy error' + err);
                  logger.error('Inside patientstudy::updatePAtientStudyInfo::Error' + err);



              } else {
                  console.log('Inside Update PatientStudy Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                  logger.info('Inside PatientStudy ::UpdatePatientStudyInfo::Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.

              }
          })
      });

  };

  var editStudyForReport = function(studyid, reportid, studystatus, userid) {
      logger.info('inside PatientStudy ::updateeditStudyForReport::assigning values');
      /* var studyid =  studyid.replace( /"/g, "" );
       var reportid =  reportid.replace( /"/g, "" );
       var userid =  userid.replace( /"/g, "" );*/

      var updatedby = userid;
      var updateddate = Date.now();

      console.log('updating values for study ::' + studyid + 'for report as ::' + reportid + 'for study staus as :' + studystatus);

      mongoose.model('PatientStudy').findById(studyid, function(err, patientstudy) {
          //update it
          patientstudy.update({
              studyreportid: reportid,
              studystatus: studystatus,
              updatedby: updatedby,
              updateddate: updateddate
          }, function(err, patientstudy) {
              if (err) {
                  console.log('Inside Update PatientStudy error' + err);
                  logger.error('Inside patientstudy::updatePAtientStudyInfo::Error' + err);



              } else {
                  console.log('Inside Update PatientStudy Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                  logger.info('Inside PatientStudy ::UpdatePatientStudyInfo::Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.

              }
          })
      });

  };




  var addStudyByUploadedFile = function(studyInfo) {
      console.log('Inside addStudyByUploadedFile' + JSON.stringify(studyInfo));
      logger.info('inside Patient ::updatePatientInfo::assigning values');
      var patientname = studyInfo.patientname;
      var age = studyInfo.age;
      var gender = studyInfo.gender;
      var studydate = currentdate;
      var studyorgan = 'studyorgan';
      var studystatus = 'New';
      var physicianname = studyInfo.physicianname;
      var physicianhospitalname = studyInfo.physicianhospitalname;
      var studyfilename = studyInfo.studyfilename;
      var studyfilestatus = studyInfo.studyfilestatus;
      var studyfileuploadedtime = studyInfo.studyfileuploadedtime;
      var zipstudyfilepath = studyInfo.zipstudyfilepath;

      //TODO add later by taking user id from login user
      // console.log('created by as ::' + req.body.createdby);


      //call the create function for our database
      mongoose.model('PatientStudy').create({
          patientname: patientname,
          age: age,

          gender: gender,
          studydate: studydate,
          studyorgan: studyorgan,
          studystatus: studystatus,
          physicianname: physicianname,
          physicianhospitalname: physicianhospitalname,
          studyfilename: studyfilename,
          studyfilestatus: studyfilestatus,
          studyfileuploadedtime: studyfileuploadedtime,
          zipstudyfilepath: zipstudyfilepath

      }, function(err, patientstudy) {
          if (err) {
              logger.error('inside patientstudy::createpatientstudy::Error' + err);
              /*   res.json({
                     header: {
                         statuscode: 400,
                         statusmessage: "failure"
                     },
                     data: {
                         errormessage: "There was a problem adding the information to the database." + err
                     }
                 });*/

          } else {
              logger.info('Inside patientstudy::addpatientstudy::Success' + patientstudy);
              //          res.json({
              // header :{ statuscode : 200 , statusmessage : "success"},
              // data : {patient : patient}    
              //add study information ferom here
              console.log('successfully created patientstudy as ::' + patientstudy);

              logger.info('Inside PAtientStudy::addPatientStudy::Success' + patientstudy);
              // res.json({
              //     header: {
              //         statuscode: 200,
              //         statusmessage: "success"
              //     },
              //     data: {
              //         patientstudy: patientstudy
              //     }
              // });//json


          } //else

      });
  }



  var getStudyByUploadedFile = function(req, res) {
      //retrieve all blobs from Monogo
      console.log('Inside searchPatientStudies');


      mongoose.model('PatientStudy').find({}).sort({
          studyfileuploadedtime: 'desc'
      }).exec(function(err, patientstudies) {

          if (err) {
              logger.error('Inside PatientStudy ::getPatientStudy::Error' + err);
              res.json({
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      errormessage: err
                  }

              });
          } //if
          else {
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              logger.info('Inside PatientStudy ::getPatientStudy::success');
              logger.info("getting PatientStudy as ::" + patientstudies);
              //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
              res.json({
                  header: {
                      statuscode: 200,
                      statusmessage: "success"
                  },
                  data: {
                      patientstudies: patientstudies
                  }
              });
          } //else 
      }); //find
  }

  var updateStudyDicomInfo = function(studyInfo) {

      console.log('Inside updateStudyDicomInfo' + JSON.stringify(studyInfo));
      mongoose.model('PatientStudy').findOne({
          studyfilename: studyInfo.studyfilename
      }, function(err, patientstudy) {
          //update it
          patientstudy.update({
              patientname: studyInfo.patientname,
              age: studyInfo.age,
              gender: studyInfo.gender,
              physicianname: studyInfo.physicianname,
              physicianhospitalname: studyInfo.physicianhospitalname,
              studystatus: studyInfo.studystatus
          }, function(err, patientstudy) {
              if (err) {
                  console.log('Inside Update PatientStudy error' + err);
                  logger.error('Inside patientstudy::updatePAtientStudyInfo::Error' + err);
              } else {
                  console.log('Inside Update PatientStudy Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                  logger.info('Inside PatientStudy ::UpdatePatientStudyInfo::Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.

              }
          });
      });


  }
  var addBatchStudyFileInfoByUpload = function(studyInfo , callback) {
      console.log('Inside addBatchStudyFileInfoByUpload');

      mongoose.model('PatientStudy').insertMany(studyInfo)
          .then(function(docs) {
              // do something with docs
              console.log('getting docs as :' + docs);
              return callback(true);
          })
          .catch(function(err) {
              // error handling here
              console.log('getting error as :' + err);
              return callback(false);
          });
  }


  var editStudyForHistoryPath = function(studyInfo, callback) {
      console.info('inside Patient ::editStudyForHistoryPath::assigning values');
      studyInfo.forEach(function(study){
    	  mongoose.model('PatientStudy').findById(study.studyid, function(err, patientstudy) {
              //update it
              patientstudy.update({
                  historypath: study.writePath,
                  updatedby: study.updatedby,
                  updateddate: Date.now()
              }, function(err, patientstudy) {
                  if (err) {
                      console.log('Inside Update editStudyForHistoryPath error' + err);
                      logger.error('Inside patientstudy::editStudyForHistoryPath::Error' + err);

                  return callback('failure');

                  } else {
                      console.log('Inside Update PatientStudy Success' + JSON.stringify(patientstudy));
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      logger.info('Inside PatientStudy ::UpdatePatientStudyInfo::Success');
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      return callback('success');
                  }
              })
          });

      });
 
  };
  var editStudyForReportPath = function(studyInfo , callback) {
      logger.info('inside Patient ::editStudyForReportPath::assigning values');
      //make this dynamic update
     var studyUpdateStatus = [];
      
      console.log('getting studyinfo as :' +JSON.stringify(studyInfo));
      studyInfo.forEach(function(study) {
    	  mongoose.model('PatientStudy').findById(study.studyid, function(err, patientstudy) {
              //update it
              patientstudy.update({
                  studyreportfilepath: study.writePath,
                  updatedby: study.updatedby,
                  updateddate: Date.now(),
                  studystatus : study.studystatus
              }, function(err, patientstudy) {
                  if (err) {
                      console.log('Inside Update editStudyForHistoryPath error' + err);
                      logger.error('Inside patientstudy::editStudyForHistoryPath::Error' + err);
                     /* 	var studyItem = {
                      			"updateStatus" : "failure",
                      			"study" : study
                      	}
                      	studyUpdateStatus.push(studyItem);*/
                      return callback('failure');
                     

                  } else {
                      console.log('Inside Update PatientStudy Success' + JSON.stringify(patientstudy));
                      //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                      logger.info('Inside PatientStudy ::UpdatePatientStudyInfo::Success');
                    /*	var studyItem = {
                      			"updateStatus" : "success",
                      			"study" : study
                      	}
                      	studyUpdateStatus.push(studyItem);*/
                      return callback('success');
        
                      
                  }
              });//update
              
          });
    	  
 
      });//foreach
      
      
    
  };

  //tODO add functionCheck.checkPermission,
  router.post('/editStudyDeleteFlag', function(req, res) {
      logger.info('Inside Patient :: addPatientStudyImageInfo');
      // Get values from POST request. These can be done through forms or REST calls. These rely on the "name" attributes for forms
      var studyid = req.body.studyid;
      var deletedby = req.body.deletedby;
      var deleteddate = Date.now();
      studyid = studyid.replace(/"/g, "");
      deletedby = deletedby.replace(/"/g, "");



      mongoose.model('PatientStudy').findById(studyid, function(err, patientstudy) {
          //update it
          patientstudy.update({
              isdelete: true,
              deletedby: deletedby,
              deleteddate: deleteddate
          }, function(err, patientstudy) {
              if (err) {
                  console.log('Inside Update editStudyDeleteFlag error' + err);
                  logger.error('Inside patientstudy::editStudyDeleteFlag::Error' + err);

                  res.json({
                      header: {
                          statuscode: 400,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: err
                      }

                  });

              } else {
                  console.log('Inside editStudyDeleteFlag Success' + JSON.stringify(patientstudy));
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                  logger.info('Inside PatientStudy ::editStudyDeleteFlag::Success');
                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
                  res.json({
                      header: {
                          statuscode: 200,
                          statusmessage: "success"
                      },
                      data: {
                          patientstudy: patientstudy
                      }
                  });
              }
          })
      });

  });




  module.exports = router;
  module.exports.editStudyForKeyword = editStudyForKeyword;
  module.exports.editStudyForReport = editStudyForReport;
  module.exports.addStudyByUploadedFile = addStudyByUploadedFile;
  module.exports.getStudyByUploadedFile = getStudyByUploadedFile;
  module.exports.addBatchStudyFileInfoByUpload = addBatchStudyFileInfoByUpload;
  module.exports.updateStudyDicomInfo = updateStudyDicomInfo;

  module.exports.editStudyForHistoryPath = editStudyForHistoryPath;
  module.exports.editStudyForReportPath = editStudyForReportPath;
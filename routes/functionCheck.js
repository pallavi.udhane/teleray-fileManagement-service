  var express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'), //mongo connection
      bodyParser = require('body-parser'), //parses information from POST
      methodOverride = require('method-override'),
      jwt = require('jsonwebtoken'),
      mongoose = require('mongoose'), //mongo connection

      RBAC = require('rbac2'),
      config = require('config'),
      logger = require('../config/logger');


  router.use(bodyParser.urlencoded({
      extended: true
  }))
  router.use(methodOverride(function(req, res) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method
          delete req.body._method
          return method
      }
  }))


  var app = express();
  var dbSecret = '';
  if (config.has('tokenConfig.secret')) {
      dbSecret = config.get('tokenConfig.secret');
  }
  app.set('superSecret', dbSecret);



  var checkPermission =
      function(req, res, next) {
          logger.info('Inside functionCheck :: checkPermission');
          var rolename = req.headers['rolename'];          
          var actionname = req.headers['actionname'];    
          var rulesForRole = req.headers['rules'];
          logger.info('Inside functionCheck :: checkPermission::ROLENAME as :' + rolename + 'ACTIONNAME as ::' + actionname + 'RULESAS::' + rulesForRole);
          var rules = eval('(' + rulesForRole + ')');
          
          var rback = new RBAC(rules);
          
        rback.check(rolename, actionname, function(err, result) {
              // result: true 
             
              if (result) {
                   logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as :' +result);
                  next();
              } else {
                 logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as false :' +err);
                  res.json({
                    
                      header: {
                          statuscode: 401,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: 'Sorry you dont have permission for ' + actionname
                      }
                    
                  });

              }
          });



      };


  var checkToken = function(req, res, next) {
       logger.info('Inside functionCheck :: checkToken');
      var token = null;
logger.info('Inside functionCheck :: checkToken::getting dbSecret');

     var bearerHeader = req.headers['authorization'];
      
      if (typeof bearerHeader !== 'undefined') {
          var bearer = bearerHeader.split(" ");
          
          token = bearer[1];
          
          
      }      
      // decode token
      if (token) {
        logger.info('Inside functionCheck :: checkToken::get token');

          // verifies secret and checks exp
          jwt.verify(token, app.get('superSecret'), function(err, decoded) {
              if (err) {
                  logger.info('Inside functionCheck :: checkToken::fails during token varification');

                  return res.json({
                      header: {
                          statuscode: 400,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: "Failed to authenticate token" + err
                      }

                  });
              } else {
                  // if everything is good, save to request for use in other routes
                  logger.info('Inside functionCheck :: checkToken::getting valid token');
                  req.decoded = decoded;
                  next();
              }
          });

      } else {

          // if there is no token
          // return an error
              return res.json({
                      header: {
                          statuscode: 403,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: "No token provided."
                      }
                  });
      }
  };

  module.exports = router;
  module.exports.checkPermission = checkPermission;
  module.exports.checkToken = checkToken;
  var express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'), //mongo connection
      bodyParser = require('body-parser'), //parses information from POST
      methodOverride = require('method-override'),
      jwt = require('jsonwebtoken'),
      mongoose = require('mongoose'), //mongo connection
      functionCheck = require('../routes/functionCheck'),
       config = require('config');
      logger = require('../config/logger'),
      multer = require('multer'),
      fs = require('fs'),
      path = require('path'),
      dicomjs = require('dicomjs'),
      patientstudy = require('../routes/patientstudy'),
      date = require('date-and-time'),
      Archiver = require('archiver'),
      AdmZip = require('adm-zip'),
      timeout = require('connect-timeout'),
      rename = require('rename'),
      filepath = require('filepath');
      


//TODO file path not get accessible over here
      var studyfilepath;
      var studyhistorypath;
      var studyreportpath;
      var studiesType;
      var studyreportType;
      var historyType;
      
      if (config.has('uploadFilePath.studyfilepath')) {
    	  studyfilepath = config.get('uploadFilePath.studyfilepath');
    	  console.log('getting  studyfilepath as :' +studyfilepath);
    	  studies = multer({
              dest: studyfilepath,
              limits: { fileSize: 200000000 }
          });
    	  studiesType = studies.any();
    	  
    	}
      if (config.has('uploadFilePath.studyhistoryfilepath')) {
    	  studyhistorypath = config.get('uploadFilePath.studyhistoryfilepath');
    	 console.log('getting history file path as :' +studyhistorypath); 
    	  studyhistorypath = multer({
    		  dest : studyhistorypath,
              limits: { fileSize: 200000000 }
    	  });
    	  historyType = studyhistorypath.any();
    	}
      if (config.has('uploadFilePath.studyreportfilepath')) {
    	  studyreportpath = config.get('uploadFilePath.studyreportfilepath');
    	  console.log('Report File Path as ::' +studyreportpath);
    	  studyreportpath = multer({
    		  dest : studyreportpath,
              limits: { fileSize: 200000000 }
    	  });
    	  studyreportType = studyreportpath.any();
    	}
      
      
  var currentdate = Date.Now;



  router.use(bodyParser.urlencoded({
      extended: true
  }))
  router.use(methodOverride(function(req, res) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method
          delete req.body._method
          return method
      }
  }));
  definitions : 
	  PatientStudy :
		  type : Object
		  properties :
			  patientname :
				  type : String
			  address : 
				  type : String
			  city :
				  type : Boolean
		      pincode :
				  type : String
			  zipstudyfilepath :
				  type : Date
			  age : 
			  	  type : String
			  agetype : 
				  type : Date
			  gender :
			  	  type : Boolean
			  dateofbirth :
			      type : String
			  mobilenumber :
				  type : String
			  emailaddress : 
				  type : String
			  historypath :
				  type : String
			  studyreportfilepath :
				  type : String
			  history : 
				  type : String
			  priliminaryreport :
				  type : String
			  centerid : 
				  type : String
			  operatorid : 
				  type : String
			  physicianname :
				  type : String
			  physicianhospitalname :
				  type : String
			  studydate : 
				  type : String
			  studyorgan :
				  type : String				  
			  studystatus :
				  type : String
			  referringphysicianname :
				  type : String
			  referringphysicianhospitalname :
				  type : String
			  comments :
				  type : String
			  description :
				  type : String
			  studyfilename :
				  type : String
		      studyfilestatus :
		    	  type : String
			  studyfileuploadedtime :
				  type : String

			      /**
			       * @swagger
			       * /fileupload/downloadStudyFile:
			       *   post:
			       *     description: download study zip file          
			       *     produces:
			       *       - application/zip
			       *     parameters:            
			       *       - name: zipstudyfilepath  
			       *         required: true
			       *         in: formData
			       *         description: file path for file
			       *         type: string
			       *       - name: Authorization  
			       *         required: true
			       *         in: header
			       *         description: authorization token pass in header
			       *         type: string
			       *       - name: rolename  
			       *         required: true
			       *         in: header
			       *         description: role name assigned to user e.g Account Admin
			       *         type: string 
			       *       - name: actionname  
			       *         required: true
			       *         in: header
			       *         description: permission name ,for user make requeste.g downloadStudyFile
			       *         type: string
			       *       - name: rules  
			       *         required: true
			       *         in: header
			       *         description: list of permissions assigned to role
			       *         type: string                             
			       *     responses:
			       *       '200':
			       *         description: file downloaded successfully
			       *       '400':
			       *         description: failure
			       *       '401':
			       *       	 description: sorry you don't have permission to downloadStudyFile  
			       *        
			       */  
  router.post('/downloadStudyFile',functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
      logger.info("Inside fileupload ::downloadStudyFile");
      var filePath = request.body.filepath;
      logger.info("Inside fileupload ::downloadStudyFile:::Filepath for download " + filePath);
      downloadFile(filePath, function(fileresponse) {
          response.send(fileresponse);
      })
  });
  
  /**
   * @swagger
   * /fileupload/downloadHistoryFile:
   *   post:
   *     description: download history file          
   *     produces:
   *       - application/json
   *     parameters:            
   *       - name: historypath  
   *         required: true
   *         in: formData
   *         description: file path for history file
   *         type: string
   *       - name: Authorization  
   *         required: true
   *         in: header
   *         description: authorization token pass in header
   *         type: string
   *       - name: rolename  
   *         required: true
   *         in: header
   *         description: role name assigned to user e.g Account Admin
   *         type: string 
   *       - name: actionname  
   *         required: true
   *         in: header
   *         description: permission name ,for user make request e.g downloadHistoryFile
   *         type: string
   *       - name: rules  
   *         required: true
   *         in: header
   *         description: list of permissions assigned to role
   *         type: String                             
   *     responses:
   *       '200':
   *         description: file download successfully
   *       '400':
   *         description: failure
   *       '401':
   *       	 description: sorry you don't have permission to downloadHistoryFile  
   *        
   */   
  
  router.post('/downloadHistoryFile',functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
      logger.info("Inside fileupload ::downloadHistoryFile");
      var filePath = request.body.filepath;
      logger.info("Inside fileupload ::downloadHistoryFile::filepath as :" + filePath);
      downloadFile(filePath, function(fileresponse) {
          response.send(fileresponse);
      });
  });
  
  /**
   * @swagger
   * /fileupload/downloadReportFile:
   *   post:
   *     description: download report file          
   *     produces:
   *       - application/json
   *     parameters:            
   *       - name: studyreportfilepath  
   *         required: true
   *         in: formData
   *         description: file path for report file
   *         type: string
   *       - name: Authorization  
   *         required: true
   *         in: header
   *         description: authorization token pass in header
   *         type: string
   *       - name: rolename  
   *         required: true
   *         in: header
   *         description: role name assigned to user e.g  Account Admin
   *         type: string 
   *       - name: actionname  
   *         required: true
   *         in: header
   *         description: permission name ,for user make request e.g downloadReportFile
   *         type: string
   *       - name: rules  
   *         required: true
   *         in: header
   *         description: list of permissions assigned to role
   *         type: string                             
   *     responses:
   *       '200':
   *         description: file download successfully
   *       '400':
   *         description: failure
   *       '401':
   *       	 description: sorry you don't have permission to downloadReportFile  
   *        
   */   
    
  router.post('/downloadReportFile',functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
      logger.info("Inside fileupload ::downloadReportFile");
      var filePath = request.body.filepath;
      logger.info("Inside fileupload ::downloadReportFile::filepath as ::" + filePath);
      downloadFile(filePath, function(fileresponse) {
          response.send(fileresponse);
      });
  });

  var downloadFile = function(filePath, callback) {
      logger.info("Inside fileupload ::downloadFile");
      fs.readFile(filePath, function(err, data) {
          if (err) {
              return logger.info("Inside fileupload ::downloadFile::Error as ::" + err);
          }
          return callback(data);
      });
  };

  var writeHistoryDescriptionInFile = function(filePath , fileContent , callback){
	  console.log('Inside writeHistoryDescriptionInFile');
	  var fs = require('fs');
	  var wstream = fs.createWriteStream(filePath);
	  wstream.write(fileContent);  
	  wstream.end();
	  return callback(true);
  };
  /**
   * @swagger
   * /fileupload/uploadStudyHistory:
   *   post:
   *     description: upload history file for study          
   *     produces:
   *       - application/json
   *     parameters:            
   *       - name: reportfile  
   *         required: true
   *         in: formData
   *         description: file path for history file
   *         type: string
   *       - name: studyid  
   *         required: true
   *         in: formData
   *         description: study id for which history get uploaded
   *         type: string
   *       - name: updatedby  
   *         required: true
   *         in: formData
   *         description: user id who uploaded this history file for study
   *         type: string    
   *       - name: Authorization  
   *         required: true
   *         in: header
   *         description: authorization token pass in header
   *         type: string
   *       - name: rolename  
   *         required: true
   *         in: header
   *         description: role name assigned to user e.g Account Admin
   *         type: string 
   *       - name: actionname  
   *         required: true
   *         in: header
   *         description: permission name as uploadStudyHistoryFile ,for user make request e.g uploadStudyHistory
   *         type: string
   *       - name: rules  
   *         required: true
   *         in: header
   *         description: list of permissions assigned to role
   *         type: string                             
   *     responses:
   *       '200':
   *         description: file download successfully
   *       '400':
   *         description: failure
   *       '401':
   *       	 description: sorry you don't have permission to uploadStudyHistoryFile  
   *        
   */   
  
  router.post('/uploadStudyHistory', historyType, functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
      logger.info("Inside fileupload ::uploadStudyHistory");
      logger.info("Inside fileupload ::uploadStudyHistory::Total files are :" + request.files.length);
      //check historydescription 
      //TODO check for historydescription
      //if this is not null write this data into file
      //save file into local disk
      
      
      
      console.log("Inside fileupload ::uploadStudyHistory::Total files are :" + request.files.length);
      if(request.files.length == 0)
    	  {
          studyhistorypath = config.get('uploadFilePath.studyhistoryfilepath');
          console.log('studyhistorypath ::' +studyhistorypath);
          //todo check for history should not null
          //if escription also null
          var updatedby = request.body.updatedby.replace(/"/g, "");
          var studyid = request.body.studyid.replace(/"/g, "");
          var historydescription = request.body.historydescription;
          console.log('getting historydescription as ::' +historydescription);
          if(historydescription === ''){
        	  response.json({
                  data :{
                      header: {
                          statuscode: 200,
                          statusmessage: "failure"
                      },
                      data: {
                          message: 'Please either select file or write history description !!!'
                      }
                  }
                      });
          } else {
          var writeStudyHistoryPath = studyhistorypath + studyid+'.txt';
          console.log('write history path becomes :' +writeStudyHistoryPath);
          var studyItem = {
                  "studyid": studyid,
                  "updatedby": updatedby,
                  "writePath": writeStudyHistoryPath ,
                  "readPath" : null
              };
          console.log('study item as :' + JSON.stringify(studyItem));
  var historyInfo =[];
  historyInfo.push(studyItem);
  writeHistoryDescriptionInFile(writeStudyHistoryPath ,historydescription,function(writeResponse){
	  if(writeResponse){
		  console.log('successfully created file....now update in database');
		  patientstudy.editStudyForHistoryPath(historyInfo,function(updateResponse){ 
			  if(updateResponse == 'success') {
				  
				  response.json({
	                  data :{
	                      header: {
	                          statuscode: 200,
	                          statusmessage: "success"
	                      },
	                      data: {
	                          message: 'successfully update history path'
	                      }
	                  }
	                      });
				  
			  }else if(updateResponse == 'failure') {
				  response.json({
	                  data : {
                      header: {
                          statuscode: 400,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: 'Problem in updating history path'
                      }
	                  }
                      });    				  
			  }
			  
		  });
		  
	  }
  });
    	  }
   }else{
	   generateFileMetaDataDetails(request,'historyFile', function(historyFileInfoResponse) {
	    	 // writeFileToStorage(historyFileInfoResponse , function(writeFileResponse){
	    		  patientstudy.editStudyForHistoryPath(historyFileInfoResponse,function(updateResponse){    			 
	    			  if(updateResponse == 'success') {
	    				  
	    				  response.json({
	    	                  data :{
	    	                      header: {
	    	                          statuscode: 200,
	    	                          statusmessage: "success"
	    	                      },
	    	                      data: {
	    	                          message: 'successfully update history path'
	    	                      }
	    	                  }
	    	                      });
	    				  
	    			  }else if(updateResponse == 'failure') {
	    				  response.json({
	    	                  data : {
	                          header: {
	                              statuscode: 400,
	                              statusmessage: "failure"
	                          },
	                          data: {
	                              errormessage: 'Problem in updating history path'
	                          }
	    	                  }
	                          });    				  
	    			  }        		  

	    		  });//updatehistrypath
	    		  
	    	  //});//writefile
	    	  
	      });//metadata

   }

  });

  /**
   * @swagger
   * /fileupload/uploadStudyReport:
   *   post:
   *     description: upload report file for study          
   *     produces:
   *       - application/json
   *     parameters:            
   *       - name: reportfile  
   *         required: true
   *         in: formData
   *         description: file path for report file
   *         type: string
   *       - name: studyid  
   *         required: true
   *         in: formData
   *         description: study id for which report get uploaded
   *         type: string
   *       - name: updatedby  
   *         required: true
   *         in: formData
   *         description: user id who uploaded this report file for study
   *         type: string    
   *       - name: Authorization  
   *         required: true
   *         in: header
   *         description: authorization token pass in header
   *         type: string
   *       - name: rolename  
   *         required: true
   *         in: header
   *         description: role name assigned to user e.g Account Admin
   *         type: string 
   *       - name: actionname  
   *         required: true
   *         in: header
   *         description: permission name as uploadStudyReport ,for user make request e.g uploadStudyReport
   *         type: string
   *       - name: rules  
   *         required: true
   *         in: header
   *         description: list of permissions assigned to role
   *         type: string                             
   *     responses:
   *       '200':
   *         description: file uploaded successfully
   *       '400':
   *         description: failure
   *       '401':
   *       	 description: sorry you don't have permission to uploadStudyReport  
   *        
   */   
 
  router.post('/testuploadStudyReport',studyreportType,functionCheck.checkToken,functionCheck.checkPermission,  function(request, response) {
  console.log('Inside testuploadStudyReport');
  console.log('Inside fileupload::uploadStudyReport::Total File as :' + request.files.length);
  
  });
  router.post('/uploadStudyReport', studyreportType, functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
	  console.log('Inside uploadStudyReport');
      logger.info('Inside fileupload::uploadStudyReport');
      logger.info('Inside fileupload::uploadStudyReport::Total File as :' + request.files.length);
      if(request.files.length == 0){
    	  response.json({
              data :{
                  header: {
                      statuscode: 400,
                      statusmessage: "failure"
                  },
                  data: {
                      message: 'Please select file for upload!!!'
                  }
              }
                  });
      } else {
      generateFileMetaDataDetails(request,'reportFile', function(reportFileInfoResponse) {
    	 // writeFileToStorage(reportFileInfoResponse , function(writeFileResponse){
    		  patientstudy.editStudyForReportPath(reportFileInfoResponse,function(updateResponse) {
    			  
    				  if(updateResponse == 'success') {
    					  console.log('getting editStudyForReportPath as ' +updateResponse);
    					  response.json({
    		                  data :{
    		                      header: {
    		                          statuscode: 200,
    		                          statusmessage: "success"
    		                      },
    		                      data: {
    		                          message: 'successfully update report path'
    		                      }
    		                  }
    		                      });
    					  
    				  }else if(updateResponse == 'failure') {
    					  response.json({
    		                  data : {
		                      header: {
		                          statuscode: 400,
		                          statusmessage: "failure"
		                      },
		                      data: {
		                          errormessage: 'Problem in updating report path'
		                      }
    		                  }
		                      });
    					  
    				  }
    			  
    				  });
    		  
    		  
    	 // });//writefilestorage
    	  
      });
      
  }

  });
  var generateFileMetaDataDetails = function(request,fileType, callback) {		
      //get all files info to store in collection	  
	  console.log('Inside generateFileMetaDataDetails file as ::');	  
	  console.log('getting fileType as :: ' + fileType);
	  var now = new Date();
      var today = date.format(now, 'YYYY/MM/DD HH:mm:ss');
      var studyMetaDataDetails = [];
     console.log('**************');
      if(fileType == 'uploadedStudyFile') {
    	  var uploadedFiles = request.files;
      uploadedFiles.forEach(function(file) {
          var studyfilename = file.originalname;      
          var studyfilestatus = 'uploaded';
          console.log('studyfilestatus :: '+studyfilestatus);
          var studyfileuploadedtime = today;
          console.log('studyfileuploadedtime as :' +studyfileuploadedtime);
          studyfilepath = config.get('uploadFilePath.studyfilepath');
          console.log('studyfilepath' + studyfilepath);
          var zipstudyfilepath = studyfilepath + file.originalname;          
          console.log('zip study file path becomes as :' + zipstudyfilepath);
          var readPath = file.path;
          console.log('readPath as ::' +readPath);
         //rename temp file as writable file    
          fs.rename(readPath, zipstudyfilepath);    
          
          
          var createdby = request.body.createdby.replace(/"/g, "");
          console.log('study item created by::' +createdby)
          var studyItem = {
              "studyfilename": studyfilename,
              "studyfilestatus": studyfilestatus,
              "studyfileuploadedtime": studyfileuploadedtime,
              "zipstudyfilepath" :zipstudyfilepath,
              "writePath": zipstudyfilepath,
              "studystatus": 'New',
              "isdelete": false,
              "readPath" : readPath,
              "createdby" : createdby
          };
          studyMetaDataDetails.push(studyItem);
          console.log('studyItem as :: ' + JSON.stringify(studyItem));
          console.log('studyMetaDataDetails as ' + JSON.stringify(studyMetaDataDetails));
      });
      }else if(fileType == 'reportFile') {
    	console.log('Inside if condition');
    	  var reportFiles = request.files;
    	  reportFiles.forEach(function(file) {
    		  console.log('Inside for loop');
              var studyfilename = file.originalname;    
              studyreportpath = config.get('uploadFilePath.studyreportfilepath');
              console.log('studyreportpath ::' +studyreportpath);
              var writeStudyReportPath = studyreportpath + studyfilename;
              console.log('studyreportpathas ::' + studyreportpath);
              var readPath = file.path;
              fs.rename(readPath, writeStudyReportPath);  
              console.log('readPath ::' + readPath);
              console.log('updataed by : ' + request.body.updatedby); 
              var updatedby = request.body.updatedby.replace(/"/g, "");
              var studyid = request.body.studyid.replace(/"/g, "");
              var studyItem = {
                      "studyid": studyid,
                      "updatedby": updatedby,
                      "writePath": writeStudyReportPath,
                      "studystatus": 'Final',
                      "readPath" : readPath

                  };
      
              studyMetaDataDetails.push(studyItem);              
              
    	  });  
      }else if(fileType == 'historyFile') {
      	console.log('Inside if condition');
  	  var historyFiles = request.files;
  	historyFiles.forEach(function(file) {
  		  console.log('Inside for loop');
            var studyfilename = file.originalname;    
            studyhistorypath = config.get('uploadFilePath.studyhistoryfilepath');
            console.log('studyreportpath ::' +studyreportpath);
            var writeStudyHistoryPath = studyhistorypath + studyfilename;
            console.log('studyhistorypath ::' + studyhistorypath);
            var readPath = file.path;
            fs.rename(readPath, writeStudyHistoryPath);  
            console.log('readPath ::' + readPath);
            console.log('updataed by : ' + request.body.updatedby); 
            var updatedby = request.body.updatedby.replace(/"/g, "");
            var studyid = request.body.studyid.replace(/"/g, "");
            var studyItem = {
                    "studyid": studyid,
                    "updatedby": updatedby,
                    "writePath": writeStudyHistoryPath  ,                  
                    "readPath" : readPath
                };
    
            studyMetaDataDetails.push(studyItem);              
            
  	  });    
    	  
      };
      
	  return callback(studyMetaDataDetails);
  };

  var writeFileToStorage = function(filesInfo , callback) {       
      /** A better way to copy the uploaded file. **/
	  console.log('Inside writeFileToStorage');	  
	  var studyWriteFilesSuccess = [];
	  var studyWriteFilesFailure = [];
	  console.log('getting file read path as ::' +file.readPath);
	  console.log('getting file write path as ::' +file.writePath);
	  fs.rename(file.readPath, file.writePath, function (err) {
		  if (err) throw err;
		  fs.stat(file.writePath, function (err, stats) {
		    if (err) throw err;
		    console.log('stats: ' + JSON.stringify(stats));
		  });
		});
	  
/*	  filesInfo.forEach(function(file) {	  
	      var src = fs.createReadStream(file.readPath);
	      var dest = fs.createWriteStream(file.writePath);
      
	       src.pipe(dest); 
	      src.on('data', function(chunk) {
	    	  dest.write(chunk);
	      });
	      
	      src.on('end', function() {
	
	    	  studyWriteFilesSuccess.push(file);
	          console.log('success in writting file on disk');
	          console.log('starts reading file');
	         
	      });
	      src.on('error', function(err) {
	          console.log('failure while writting file to disk' + err);         
	          studyWriteFilesFailure.push(file);
	      });  
      
	  });
	  var studyWriteFiles = {
			  "successWriteFiles" : studyWriteFilesSuccess , 
			  "failureWriteFiles" : studyWriteFilesFailure
	  }*/
	  return callback(studyWriteFiles);
  };
  
  /**
   * @swagger
   * /fileupload/uploadStudyZipFiles:
   *   post:
   *     description: upload study zip file           
   *     produces:
   *       - application/json
   *     parameters:            
   *       - name: file  
   *         required: true
   *         in: formData
   *         description: file path for study zip file
   *         type: string
   *       - name: updatedby  
   *         required: true
   *         in: formData
   *         description: user id who uploaded this zip file
   *         type: string    
   *       - name: Authorization  
   *         required: true
   *         in: header
   *         description: authorization token pass in header
   *         type: string
   *       - name: rolename  
   *         required: true
   *         in: header
   *         description: role name assigned to user e.g Account Admin / Ass.Operator
   *         type: string 
   *       - name: actionname  
   *         required: true
   *         in: header
   *         description: permission name as uploadStudyReport ,for user make request e.g uploadStudyZipFiles
   *         type: string
   *       - name: rules  
   *         required: true
   *         in: header
   *         description: list of permissions assigned to role
   *         type: string                             
   *     responses:
   *       '200':
   *         description: file uploaded successfully
   *       '400':
   *         description: failure
   *       '401':
   *       	 description: sorry you don't have permission to uploadStudyZipFiles  
   *        
   */   
   
  router.post('/uploadStudyZipFiles', studiesType, functionCheck.checkToken,functionCheck.checkPermission, function(request, response) {
	  logger.info('Inside fileupload :: uploadStudyZipFiles');
	  console.log('getting files as ::' + request.files);
	  console.log('after timeout');
      logger.info('Inside fileupload :: uploadStudyZipFiles::total file are' + request.files.length);

      response.setTimeout(300000, function(){
    	  console.log(' Timeout Happens !!! ');
      });
      
      var studies = [];
   if(request.files.length == 0){
	   response.json({
           data :{
               header: {
                   statuscode: 400,
                   statusmessage: "failure"
               },
               data: {
                   message: 'Please select file for upload!!!'
               }
           }
               });
   } else {
      generateFileMetaDataDetails(request,'uploadedStudyFile', function(uploadFileInfoResponse) {
    	
    	//  writeFileToStorage(uploadFileInfoResponse , function(writeFileResponse){
    		  
//    		  var filesgetfailinWrite = writeFileResponse.failureWriteFiles;
//    		  console.log('failure file list as :' +filesgetfailinWrite);
//    		  
//    		  var filesgetsuccessinWrite = writeFileResponse.successWriteFiles;
//    		  console.log('success file list as :' +filesgetsuccessinWrite);
    		  //insert into database
    		  patientstudy.addBatchStudyFileInfoByUpload(uploadFileInfoResponse , function(saveResponse){
    			  console.log('add batch reposnse as ::' +saveResponse);
    			  
    			  if(saveResponse)
    				  {
    				  var studyitem = {
    						  "studysavestatus" : "success" , 
    						  "studies" : uploadFileInfoResponse
    				  }
    				  studies.push(studyitem);    				  
    		
    				      
    				  }
    			  else
    				  {
    				  var studyitem = {
    						  "studysavestatus" : "failure" , 
    						  "studies" : uploadFileInfoResponse
    				  }
    				  studies.push(studyitem);
    			
    				  }
    			     //return response
    			  
				    response.json({
	                  
	                      header: {
	                          statuscode: 200,
	                          statusmessage: "success"
	                      },
	                      data: {
	                    	  studies: studies
	                      }
	                      

	                  });
    		      console.log('Studies becomes as ::' + JSON.stringify(studies));
    			 
    		  });
    		  
    		  
    	 // });//write file storage
      });//generatefilemetadata

    //  });//newlyuploadedfile
      
  }
  });
  
  var unzipFolder = function(target_path, callback) {
      logger.info('Inside fileupload :: unzipfolder');
      var zip = new AdmZip(target_path);
      var zipEntries = zip.getEntries(); // an array of ZipEntry records 
      var dicomfilename;
      zipEntries.forEach(function(zipEntry) {
          dicomfilename = zipEntry.entryName;
          logger.info('Inside fileupload :: unzipfolder::getting dicomfilename as ::' + dicomfilename);

      }); //loop
      var res = target_path.split('.zip');
      console.log(JSON.stringify(res));
      var extractPath = res[0];
      zip.extractAllTo( /*target path*/ extractPath, /*overwrite*/ true);
      var result = {
          "dicomfilename": dicomfilename,
          "extractPath": extractPath
      }

      return callback(result);
  };
  module.exports = router;
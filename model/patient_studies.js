// grab the things we need
var mongoose = require('mongoose');
//var autoIncrement = require('mongoose-auto-increment');


var patientStudiesSchema = new mongoose.Schema({  
  //patientstudyid : {type: Number, required: true , unique : true},
  patientname : String ,
  address : String ,
  city : String ,
  pincode : String , 
  zipstudyfilepath : String ,
 age : String , 
 agetype : String, 
  gender : Boolean ,
  dateofbirth :String ,
  mobilenumber : String ,
  emailaddress : String ,
  historypath : String ,
  studyreportfilepath : String ,
  history : String ,
  priliminaryreport : String ,  
    centerid : {type: mongoose.Schema.Types.ObjectId, ref: 'Center'},
  operatorid : {type: mongoose.Schema.Types.ObjectId, ref: 'User'} ,
  physicianname : String ,
  physicianhospitalname : String ,
  studydate : {type : Date , default : Date.now} ,
  studyorgan : String,
  studystatus : String ,
  modalityid : Number ,
  referringphysicianname : String,
  referringphysicianhospitalname : String,
  comments : String,
  description : String,
  images :[{
  	imagepath : String
  }],
  studyfilename : String , 
  studyfilestatus : String , 
  studyfileuploadedtime : String,
  
	keywordid : {type: mongoose.Schema.Types.ObjectId, ref: 'Keyword'} ,
  
   markstatus : Boolean ,
  studyreportid : {type: mongoose.Schema.Types.ObjectId, ref: 'StudyReport'} ,
  createdby : {type: mongoose.Schema.Types.ObjectId, ref: 'User'} ,
  createddate : { type: Date, default: Date.now } , 
  updatedby : {type: mongoose.Schema.Types.ObjectId, ref: 'User'} ,
  updateddate : Date ,
isdelete : Boolean,
   deletedby : {type: mongoose.Schema.Types.ObjectId, ref: 'User'} ,
  deleteddate : Date 
});

// autoIncrement.initialize(mongoose.connection);
// patientStudiesSchema.plugin(autoIncrement.plugin , {model : 'PatientStudy' , field : 'patientstudyid'});
mongoose.model('PatientStudy', patientStudiesSchema);
module.exports = patientStudiesSchema;